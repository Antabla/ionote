import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
//importamos los providers que nos, proveeran de las funciones necesarios para autenticarnos y 
// las funciones del CRUD
import { AuthProvider } from '../../providers/auth/auth';
import { FirebaseDbProvider } from '../../providers/firebase-db/firebase-db';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  //Creamos una variable de tipo any, en la cual guardaremos todas las notas
  //existentes en nuestra base de datos de Firebase, para luego poder mostrarlas
  // y trabajar con ellas
  notas: any;

  //En el constructor definimos todos los objetos con los que trabajaremos los cuales seran, los dos providers que
  //creamos y el objeto ModalController para poder crear ventanas tipo modal
  constructor(public modalCtrl: ModalController, public auth: AuthProvider, public dbFirebase: FirebaseDbProvider) { }

  //Cuando la aplicacion arranque lo primero que hara sera obtener todas las notas que estan guardadas en Firebase
  //y asignarselas a nuestra variable notas, para que se visualicen en la aplicacion y poder hacer operaciones
  //crud en ellas
  ionViewDidEnter() {
    //Llamamos a la funcion getNotas del provider y una ves que obtenga las notas, se las asigna a la variable
    //notas
    this.dbFirebase.getNotas().subscribe(notas => {
      this.notas = notas;
    })
  }


  //Definimos una funcion cerrar sesion que se encarga de llamar a la funcion logOut del provider de autentificacion
  cerrarSesion() {
    this.auth.logout();
  }


  //Definimos una funcion nueva nota la cual lo que hace es crear y mostrar un modal el cual tomara los 
  //datos de cada nota, le pasamos al modal una nota vacia
  nuevaNota() {
    this.modalCtrl.create('VerGuardarPage', {id: null, titulo: null, texto: null}).present();
  }

  //Definimos una funcion la cual se encarga de eliminar una nota
  //esta recibe de parametro un nota con todos sus atributos
  //luego hacemos un llamadao a la funcion borrarNota de nuestro provider
  //pasandole el id de la nota en cuestion
  eliminarNota(nota) {
    this.dbFirebase.borrarNota(nota.id);
  }

  //Definimos una funcion la cual le permite al usuario editar una nota
  //esta crea y mostra un modal el cual editaremos los
  //datos de la nota, le pasamos al modal los datos de la nota en cuestion
  editarNota(nota) {
    this.modalCtrl.create('VerGuardarPage', {id: nota.id, titulo: nota.titulo, texto: nota.texto}).present();
  }

}
