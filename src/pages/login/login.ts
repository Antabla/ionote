//Controlador de la ventana login
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

//Importamos el provider para hacer uso de la autenticacion de firebase
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  //Definimos una variable usuario la cual tendra un email y un password, en un  principio vacios
  user = { email: '', password: '' };

  //Definimos los objetos que usaremos en nuestro constructor
  //Como lo son el provider de autentificacion y un Alert controller 
  //para mostrar mensajes de errores por pantalla
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthProvider,
    public alertCtrl: AlertController
  ) {

  }

  //Definimos una funcion para registrar usarios nuevos en la app
  signin() {
    //Llamamos a la funcion registerUser la cual registra un usuario con su email y una contrasena
    this.auth.registerUser(this.user.email, this.user.password)
      .then((user) => {
        // El usuario se ha creado correctamente
      })
      //si no, ha ocurrido un error en la creacion y el catch lo atrapa
      //y muestra un mensaje de error a traves de un alert
      .catch(err => {
        //creamos una alerta la cual tendra un titulo, un texto de error y un boton para cerrar el mismo
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: err.message,
          buttons: ['Aceptar']
        });
        alert.present();
      })

  }

  //Definimos una funcion login, la cual le permimtira a un usuario inciar sesion en la aplicacion
  //esta llama a la funcion LoginUser del provider de autentificacion y le pasa los datos delusuario
  login() {
    this.auth.loginUser(this.user.email, this.user.password).then((user) => {
      //si llega aqui es porque se autentifico
    }
    )
    //si hubo un error lo muestra a traves de un alert
      .catch(err => {
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: err.message,
          buttons: ['Aceptar']
        });
        alert.present();
      })
  }

}