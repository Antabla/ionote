import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerGuardarPage } from './ver-guardar';

@NgModule({
  declarations: [
    VerGuardarPage,
  ],
  imports: [
    IonicPageModule.forChild(VerGuardarPage),
  ],
})
export class VerGuardarPageModule {}
