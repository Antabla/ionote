//Controlador del modal Ver-Guardar
//el cual le permite al usuario guardar nuevas notas y editar existentes
import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';
//Importamos el provider de firebase el cual contiene las funciones CRUD 
import { FirebaseDbProvider } from '../../providers/firebase-db/firebase-db';


@IonicPage()
@Component({
  selector: 'page-ver-guardar',
  templateUrl: 'ver-guardar.html',
})
export class VerGuardarPage {

  //Definimos un objeto nota el cual tendra los atributos que toda nota debe tener para ser guardados en la base de datos
  nota: any = {
    id: null,
    titulo: null,
    texto: null,
    fecha: null
  }

  //Definimos las objetos que usaremos en este caso el provider de firebase para las operaciones CRUD
  constructor(public viewCtrl:ViewController, public navParams: NavParams, public dbFirebase: FirebaseDbProvider) {
  }

  //Cuando el modal sea llamado el cargara los datos de la nota que se le haya pasado
  ionViewDidLoad() {
    this.nota.id = this.navParams.get('id');
    this.nota.titulo = this.navParams.get('titulo');
    this.nota.texto = this.navParams.get('texto');

  }

  //Definimos una funcion guardar la cual nos permitira guardar una nota u editarla segun sea el caso
  guardar(){
    //Le asignamos la fecha de modificacion a la nota como la fecha actual del sistema
    this.nota.fecha = Date.now();
    //si el id de la nota esta vacio significa que es una nueva nota y por lo tanto se llamara a la funcion guardar
    //si no se llamara a la funcion editar
    if(this.nota.id == null){
      //llamamos a la funcion guardaNota del provider y le pasamos la nota correspondiente
      this.dbFirebase.guardaNota(this.nota).then(res => {
        //si la operacion se completa con exito muestra un mensaje por consola
        //de todas formas si la accion se completa con exito, la lista de notas se actualizara de inmediato
        console.log('Tarea guardada');
      })
    }else{
      //si es una nota ya existente, llamamos a la funcion editarNota del provider y le pasamos la nota con los
      //valores modificados
      this.dbFirebase.editarNota(this.nota);
    }
    this.cerrar();
  }

  //Definimos una funcion para cerrar el modal
  cerrar(){
    this.viewCtrl.dismiss();
  }

}
