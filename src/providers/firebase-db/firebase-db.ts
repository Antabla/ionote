import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AuthProvider } from '../auth/auth';
/*
  Generated class for the FirebaseDbProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class FirebaseDbProvider {

  constructor(public afDB: AngularFireDatabase, public auth: AuthProvider) {
  }

  guardaNota(nota) {
    nota.id = Date.now();
    return this.afDB.database.ref('notas/' + this.auth.getUser() + '/' + nota.id).set(nota)
  }

  getNotas() {
    return this.afDB.list('notas/' + this.auth.getUser()).valueChanges();
  }

  borrarNota(id) {
    this.afDB.database.ref('notas/' + this.auth.getUser() + '/' + id).remove();
  }

  editarNota(nota){
    return this.afDB.database.ref('notas/' + this.auth.getUser() + '/' + nota.id).set(nota)
  }

}