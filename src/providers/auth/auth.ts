/*
Provider de autentificacion el cual nos proporcionara las funciones necesarias para autentificarnos
*/
import { Injectable } from '@angular/core';
//importamos la clase de autentifiacion de angularfire2
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AuthProvider {

  //Definimos en el constructor los objetos que usaremos en este caso
  //un objeto de la clase auth de angularfire2
  constructor(private afAuth: AngularFireAuth) {
  }

  /**
   * Definimos una funcion con la cual registraremos un usuario en la aplicacion
   * esta funcion recibe dos parametros el email y el password
   */
  registerUser(email: string, password: string) {
    //hacemos un llamadoa a la funcion createUserWithEmailAndPassword y le pasamos el email y el pass
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((res) => {
        //si el registro es exitoso de inmediato le hacemos un login al usuario en la app
        this.loginUser(email, password)
      })
      
  }

  //Definimos una funcion que hara un login del usuario
  loginUser(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
    .then(
      //si el login es exitoso le abrimos una sesion a ese usuario
      user => Promise.resolve(user)
      )
    .catch(
      //si falla el login no creamos ninguna sesion
      err => Promise.reject(err))
  }

  // Logout de usuario
  logout() {
    this.afAuth.auth.signOut().then(() => {
      // hemos salido
    })
  }

  // Devuelve la session
  get Session() {
    return this.afAuth.authState;
  }

  // Obtenemos el id de usuario, para poder guardar sus notas.
  getUser() {
    return this.afAuth.auth.currentUser.uid;
  }

}