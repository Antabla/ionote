import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//importamos las dependencias de autenticacion de firebase para poder iniciar sesion en la app
import { AuthProvider } from '../providers/auth/auth';

import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  //Definimos como pagina principal la pagina de inicio de sesion
  rootPage: any = 'LoginPage';

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private auth: AuthProvider) {
    platform.ready().then(() => {
      //validamos si una sesion esta activa o no
      this.auth.Session.subscribe(session => {
        if (session) {
          //si una sesion esta activa que nos redirija a la pagina principal
          this.rootPage = HomePage;
        }
        else {
          //si no hay sesiones activas que nos lleve a la pagina de inicio de sesion
          this.rootPage = 'LoginPage';
        }
      });
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

