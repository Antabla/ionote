import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

//importamos todas las dependecias de firebase necesarias para poder hacer uso de Firebase en nuestra app
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AuthProvider } from '../providers/auth/auth';
import { FirebaseDbProvider } from '../providers/firebase-db/firebase-db';

//Definimos la configuracion de acceso que nos provee Firebase para poder conectarnos a nuestro base de datos
export const firebaseConfig = {
    apiKey: "AIzaSyBIMByoariSJcHcPCVoqvjRFs6NjR67FKM",
    authDomain: "firenotes-c011a.firebaseapp.com",
    databaseURL: "https://firenotes-c011a.firebaseio.com",
    projectId: "firenotes-c011a",
    storageBucket: "firenotes-c011a.appspot.com",
    messagingSenderId: "231757994987"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    //inicializamos fireModule con nuestra configuracion de FireBase e importamos las demas clases
    //para poder hacer uso de las funciones de firebase
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    //hacemos un llamado a los providers que necesitaremos para autentificarnos y para hacer uso
    // de las funciones CRUD de nuestra app
    AuthProvider,
    FirebaseDbProvider
  ]
})
export class AppModule {}
